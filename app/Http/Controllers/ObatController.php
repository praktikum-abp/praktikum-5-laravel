<?php

namespace App\Http\Controllers;

use App\Models\Medicine;
use Illuminate\Http\Request;

class ObatController extends Controller
{

    public function index()
    {
        $data['medicines'] = Medicine::all();
        return view('pages.medicines', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'medicine_code' => 'required',
            'medicine_name' => 'required',
            'medicine_desc' => 'required',
            'medicine_type' => 'required|in:vitamin,kapsul',
            'medicine_amount' => 'required',
            'medicine_price' => 'required'
        ]);

        $new_medicine = new Medicine;
        $new_medicine->kode = $request->medicine_code;
        $new_medicine->nama = $request->medicine_name;
        $new_medicine->desc = $request->medicine_desc;
        $new_medicine->tipe = $request->medicine_type;
        $new_medicine->jumlah = $request->medicine_amount;
        $new_medicine->harga = $request->medicine_price;

        if ($new_medicine->save()) {
            return redirect(route('medicine'));
        }
    }

    public function delete($kode)
    {
        $medicine = Medicine::where('kode', $kode)->first();

        if ($medicine != null) {
            $medicine->delete();

            return redirect(route('medicine'));
        }
    }

    public function edit(Request $request, $kode)
    {
        $request->validate([
            'medicine_code' => 'required',
            'medicine_name' => 'required',
            'medicine_desc' => 'required',
            'medicine_type' => 'required|in:vitamin,kapsul',
            'medicine_amount' => 'required',
            'medicine_price' => 'required'
        ]);

        $medicine = Medicine::where('kode', $kode)->first();

        if ($medicine != null) {
            $medicine->kode = $request->medicine_code;
            $medicine->nama = $request->medicine_name;
            $medicine->desc = $request->medicine_desc;
            $medicine->tipe = $request->medicine_type;
            $medicine->jumlah = $request->medicine_amount;
            $medicine->harga = $request->medicine_price;

            if ($medicine->save()) {
                return redirect(route('medicine'));
            }
        }
    }

}
