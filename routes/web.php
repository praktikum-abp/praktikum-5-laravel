<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/lat1', [Lat1Controller::class, 'index']);
Route::get('/lat1/m2', [Lat1Controller::class, 'method2']);


Route::prefix('dashboard')->group(function () {
    Route::prefix('medicines')->group(function() {
        Route::get('/', [ObatController::class, 'index'])->name('medicine');
        Route::post('/', [ObatController::class, 'store'])->name('medicine.store');
        Route::put('/{kode}', [ObatController::class, 'edit'])->name('medicine.edit');
        Route::delete('/{kode}', [ObatController::class, 'delete'])->name('medicine.delete');
    });
});
