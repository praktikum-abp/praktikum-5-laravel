@extends('layouts.app')

@section('title', 'Medicines')

@section('extra-javascript')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://use.fontawesome.com/ba689e9e78.js"></script>
    <script>
        $(document).ready(function () {
            @if($errors->any())
                $("#error-toast").toast("show");
            @endif
        });

        $(document).on("click", ".btn-delete", function () {
            var code = $(this).data('code');
            var name = $(this).data('name');

            $("#delete-message-modal").text(`Yakin ingin menghapus ${name} dengan kode ${code}?`);
            $("#delete-modal form").attr('action', `{{ route('medicine.store') }}/${code}`);

            // show modal
            $('#delete-modal').modal('show');
        });

        $(document).on("click", ".btn-edit", function () {
            var medicine = $(this).data('medicine');

            $("#form-title").text("Ubah Data Obat");
            $("#input-code-medicine").val(medicine.kode);
            $("#input-name-medicine").val(medicine.nama);
            $("#input-desc-medicine").val(medicine.desc);
            $("#input-type-medicine").val(medicine.tipe).change();
            $("#input-amount-medicine").val(medicine.jumlah);
            $("#input-price-medicine").val(medicine.harga);
            $("#medicine-form form").attr('action', `{{ route('medicine.store') }}/${medicine.kode}`);
            $("#medicine-form form").append("<input name='_method' value='PUT' type='hidden'>")

            // show modal
            $('#medicine-form').modal('show');
        });
    </script>
@endsection

@section('content')

    @if ($errors->any())
        {{-- error toast --}}
        @component('components.toast', ['title' => "Errors"])
            @slot('message')
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            @endslot
        @endcomponent
    @endif

    @component('components.modal', [
            'route' => route('medicine.store'),
            'method' => 'POST',
            'id' => 'medicine-form',
        ])
        @slot('header')
            <h1 class="modal-title fs-5" id="form-title">Tambah Obat</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        @endslot
        @slot('body')
            <div class="mb-3">
                <label for="input-code-medicine" class="form-label">Kode Obat</label>
                <input type="text" name="medicine_code" id="input-code-medicine" class="form-control">
            </div>
            <div class="mb-3">
                <label for="input-name-medicine" class="form-label">Nama Obat</label>
                <input type="text" name="medicine_name" id="input-name-medicine" class="form-control">
            </div>
            <div class="mb-3">
                <label for="input-desc-medicine" class="form-label">Deskripsi</label>
                <textarea name="medicine_desc" id="input-desc-medicine" cols="20" rows="5"
                          class="form-control"></textarea>
            </div>
            <div class="mb-3">
                <label for="input-type-medicine" class="form-label">Tipe Obat</label>
                <select name="medicine_type" id="input-type-medicine" class="form-control">
                    <option value="">---pilih tipe obat---</option>
                    <option value="kapsul">Kapsul</option>
                    <option value="vitamin">Vitamin</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="input-amount-medicine" class="form-label">Jumlah Obat</label>
                <input type="number" name="medicine_amount" id="input-amount-medicine" class="form-control">
            </div>
            <div class="mb-3">
                <label for="input-price-medicine" class="form-label">Harga Obat</label>
                <input type="number" name="medicine_price" id="input-price-medicine" class="form-control">
            </div>
        @endslot
    @endcomponent

    <!-- form modal for delete confirmation -->
    @component('components.modal', [
        'route' => route('medicine.delete', ['kode' => -1]),
        'method' => 'DELETE',
        'id' => "delete-modal",
        'color' => "danger",
        'confirmation' => "Hapus Data"
    ])
        @slot('header')
            <h1 class="modal-title fs-5" id="delete-title-modal">Hapus Obat</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        @endslot
        @slot('body')
            <p id="delete-message-modal">Yakin akan menghapus obat?</p>
        @endslot
    @endcomponent

    <button type="button" class="btn btn-primary mt-4 mb-3" data-bs-toggle="modal" data-bs-target="#medicine-form">
        Tambah Obat
    </button>

    <table class="table">
        <thead>
        <th>Kode</th>
        <th>Nama</th>
        <th width="400">Deskripsi</th>
        <th>Tipe</th>
        <th>Jumlah</th>
        <th>Harga Satuan</th>
        <th>Aksi</th>
        </thead>
        <tbody>
        @foreach($medicines as $medicine)
            <tr>
                <td>{{ $medicine->kode }}</td>
                <td>{{ $medicine->nama }}</td>
                <td>{{ $medicine->desc }}</td>
                <td>{{ $medicine->tipe }}</td>
                <td>{{ $medicine->jumlah }}</td>
                <td>{{ $medicine->harga }}</td>
                <td>
                    <!-- Delete -->
                    <button type="button" class="btn btn-danger btn-delete"
                            data-code="{{ $medicine->kode }}"
                            data-name="{{ $medicine->nama }}"
                            data-bs-toggle="modal"
                            data-bs-target="#delete-modal">
                        <i class="fa fa-trash"></i>
                    </button>

                    <!-- Edit -->
                    <button type="button" class="btn btn-warning btn-edit"
                            data-medicine="{{ json_encode($medicine) }}"
                            data-bs-toggle="modal"
                            data-bs-target="#medicine-form">
                        <i class="fa fa-edit"></i>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
