<div class="modal fade" id="{{ $id ?? "form-modal" }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ $route }}" method="{{ $htmlMethod ?? "post" }}">
                {{ csrf_field() }}
                @method($method)
                <div class="modal-header">
                    {!! $header !!}
                </div>
                <div class="modal-body">
                    {!! $body !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-{{ $color ?? "primary" }}">{{ $confirmation ?? "Simpan" }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
